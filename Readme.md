# This is homework project for implementing QuickSort algorithm.

## Building
You'll need to have cmake >= 3.5 installed and added to your path.
Open your terminal in root of this repository and run the following code:

```bash
mkdir build
cd build
cmake ..
```
Built project files will be placed in "build" folder.
Note: If you are on macOS you might want to run cmake .. -G"Xcode" to generate Xcode project.  

### Remember - building in release mode will yield the best performance.  

## Entities

### sorting_library  
This library implements quicksort algorithm and is used in all other applications.

### generator_library  
This library provides a convinient method for test data generation in large quantities.

### data_generator  
This executable allows to take a look at generated data, and if if necessary - change generation parameters.  
Usage: call ```data_generator -h``` to get more details.  
Dependencies: ```generator_library```.  

### sorter
This executable allows to perform sorting of data specified in ```duomenys.txt``` and stores output to file ```rezultatas.txt```.  
Allows CLI user interaction.  
Dependencies: ```sorting_library```.  

### benchmark  
This executable was made to benchmark QuickSort algorithm performance with different size data sets.  
Internally generates a sequence of numbers using uniform random algorithm.  
Usage: call ```benchmark -h``` to get more details.  
Dependencies: ```generator_library```,```sorting_library```.  
