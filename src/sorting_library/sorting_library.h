#pragma once

#include <vector>

int partition(std::vector<int>& vec, int low, int high, bool ascending = true);

void quickSort(std::vector<int>& vec, int low, int high, bool ascending = true);
