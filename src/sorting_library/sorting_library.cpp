#include "sorting_library.h"

int partition(std::vector<int>& vec, int low, int high, bool ascending)
{
    int x = vec[low];
    int i = low;
    int j;
    
    for (j = low + 1; j < high; j++)
    {
        if ((vec[j] >= x) ^ ascending)
        {
            i = i + 1;
            std::swap(vec[i], vec[j]);
        }
    }
    
    std::swap(vec[i], vec[low]);
    return i;
}

void quickSort(std::vector<int>& vec, int low, int high, bool ascending)
{
    int pivot;
    if (low < high)
    {
        pivot = partition(vec, low, high, ascending);
        quickSort(vec, low, pivot, ascending);
        quickSort(vec, pivot + 1, high, ascending);
    }
}
