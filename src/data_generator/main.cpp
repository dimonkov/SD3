#include <iostream>
#include <stdlib.h>
#include <string>
#include "generator_library.h"

using namespace std;

int main(int argc, char* argv[])
{
    std::ios::sync_with_stdio(false);
    int n = 0,r = INT_MAX, seed = 0;
    for (int i = 0; i < argc; i++) {
        
        if(string(argv[i]) == "-h")
        {
            printf("\nOptions: \n");
            printf("-h Prints this screen\n");
            printf("-n [size] allows to specify size of generated collection.\n");
            printf("-r [max] allows to specify generation range.\n");
            printf("-s [seed] allows to specify RNG seed.\n");
            exit(EXIT_SUCCESS);
        }
        
        if(string(argv[i]) == "-n")
        {
            if (i + 1 < argc)
            {
                n = atoi(argv[++i]);
            }
            else
            {
                fprintf(stderr,"-n option requires generated length sequence to be specified\n");
                exit(EXIT_FAILURE);
            }
        }
        if(string(argv[i]) == "-r")
        {
            if (i + 1 < argc)
            {
                r = atoi(argv[++i]);
            }
            else
            {
                fprintf(stderr,"-r option requires greatest number you wish to generate as a parameter\n");
                exit(EXIT_FAILURE);
            }
        }
        
        if(string(argv[i]) == "-s")
        {
            if (i + 1 < argc)
            {
                seed = atoi(argv[++i]);
            }
            else
            {
                fprintf(stderr,"-s option requires you to pass integer seed as a parameter\n");
                exit(EXIT_FAILURE);
            }
        }
        
    }
    
    vector<int> vec;
    
    fill_with_uniform_random(vec, n, 0, r, seed);
    
    for (int i = 0; i < n; i++) {
        printf("%d ",vec[i]);
    }
    printf("\n");
    
    exit(EXIT_SUCCESS);
}
