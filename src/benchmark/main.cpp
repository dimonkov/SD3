#include <iostream>
#include <stdlib.h>
#include <string>
#include <chrono>
#include "generator_library.h"
#include "sorting_library.h"

using namespace std;
using namespace std::chrono;

struct Parameters {
    int n;
    int range;
    int seed;
    bool ascending_sorting_order;
    
    Parameters()
    {
        n = 0;
        range = INT_MAX;
        seed = 0;
        ascending_sorting_order = true;
    }
};

Parameters parse_cli_parameters(int argc, char* argv[]);

int main(int argc, char* argv[])
{
    //Disable sync for speed
    std::ios::sync_with_stdio(false);
    
    //Parse CLI parameters, to allow benchmark customization
    Parameters p = parse_cli_parameters(argc, argv);
    
    vector<int> vec;
    
    printf("\nGenerating test data...\n");
    //Prepare benchmark test data
    fill_with_uniform_random(vec, p.n, 0, p.range, p.seed);
    
    printf("Test data generated...\n");
    printf("Starting the benchmark...\n");
    //Capture benchmark start time
    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    
    //Run benchmarked function
    quickSort(vec, 0, p.n,p.ascending_sorting_order);
    
    //Capture benchmark execution end time
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    
    auto duration = duration_cast<microseconds>( t2 - t1 ).count();
    printf("Benchmark took: %lld microseconds\n", duration);
    
    exit(EXIT_SUCCESS);
}

Parameters parse_cli_parameters(int argc, char* argv[])
{
    Parameters p;
    
    for (int i = 0; i < argc; i++)
    {
        if(string(argv[i]) == "-h")
        {
            printf("\nOptions: \n");
            printf("-h Prints this screen\n");
            printf("-n [size] allows to specify size of generated collection.\n");
            printf("-r [max] allows to specify generation range.\n");
            printf("-rev allows to reverse sorting order (default: ascending).\n");
            printf("-s [seed] allows to specify RNG seed.\n");
            exit(EXIT_SUCCESS);
        }
        
        if(string(argv[i]) == "-n")
        {
            if (i + 1 < argc)
            {
                p.n = atoi(argv[++i]);
            }
            else
            {
                fprintf(stderr,"-n option requires generated length sequence to be specified\n");
                exit(EXIT_FAILURE);
            }
        }
        if(string(argv[i]) == "-r")
        {
            if (i + 1 < argc)
            {
                p.range = atoi(argv[++i]);
            }
            else
            {
                fprintf(stderr,"-r option requires greatest number you wish to generate as a parameter\n");
                exit(EXIT_FAILURE);
            }
        }
        
        if(string(argv[i]) == "-rev")
        {
            p.ascending_sorting_order = false;
        }
        
        if(string(argv[i]) == "-s")
        {
            if (i + 1 < argc)
            {
                p.seed = atoi(argv[++i]);
            }
            else
            {
                fprintf(stderr,"-s option requires you to pass integer seed as a parameter\n");
                exit(EXIT_FAILURE);
            }
        }
    }
    return p;
}

