project(benchmark)

# compile main executable
add_executable(benchmark main.cpp)

# add library header files
target_include_directories(
    benchmark
    PRIVATE
        ${PROJECT_BINARY_DIR}
	${generator_library_SOURCE_DIR}
	${sorting_library_SOURCE_DIR}
    )


# link libraries
target_link_libraries(benchmark generator_library)
target_link_libraries(benchmark sorting_library)
