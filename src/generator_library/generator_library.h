#pragma once

#include <vector>
#include <climits>

void fill_with_uniform_random(std::vector<int>& vec, int n, int from = INT_MIN, int to = INT_MAX,int seed = 0);
