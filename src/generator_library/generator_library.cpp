#include "generator_library.h"
#include <random>

void fill_with_uniform_random(std::vector<int>& vec, int n, int from, int to,int seed)
{
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> distribution(from,to);
    
    for (int i = 0; i < n; i++) {
        vec.push_back(distribution(generator));
    }
}
